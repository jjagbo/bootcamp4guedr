package io.keepcoding.guedr.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import io.keepcoding.guedr.R;
import io.keepcoding.guedr.fragment.CityListFragment;
import io.keepcoding.guedr.fragment.CityPagerFragment;
import io.keepcoding.guedr.model.Cities;
import io.keepcoding.guedr.model.City;

public class ForecastActivity extends AppCompatActivity implements CityListFragment.OnCitySelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Chuleta para saber detalles del dispositivo real que está ejecutando esta aplicación
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        int dpWidth = (int) (width / metrics.density);
        int dpHeight = (int) (height / metrics.density);
        String model = Build.MODEL;
        int dpi = metrics.densityDpi;

        setContentView(R.layout.activity_forecast);

        // Cargamos a mano el fragment
        FragmentManager fm = getFragmentManager();

        // Averiguamos qué interfaz hemos cargado
        // Eso lo averiguamos preguntando si en la interfaz tenemos un FrameLayout concreto
        if (findViewById(R.id.city_list_fragment) != null) {
            // Hemos cargado una interfaz que tiene el hueco para el fragment CityListFragment
            // Comprobamos primero que no tenemos ya añadido el fragment a nuestra jerarquía
            if (fm.findFragmentById(R.id.city_list_fragment) == null) {
                // No está añadido, lo añadiré con una transacción de fragments
                //Creo la instancia del nuevo fragment
                Cities cities = Cities.getInstance();
                CityListFragment fragment = CityListFragment.newInstance(cities.getCities());

                fm.beginTransaction()
                        .add(R.id.city_list_fragment, fragment)
                        .commit();
            }
        }

        if (findViewById(R.id.view_pager_fragment) != null) {
            // Hemos cargado una interfaz que tiene el hueco para el fragment CityPagerFragment
            // Comprobamos primero que no tenemos ya añadido el fragment
            if (fm.findFragmentById(R.id.view_pager_fragment) == null) {
                fm.beginTransaction()
                        .add(R.id.view_pager_fragment, CityPagerFragment.newInstance(0))
                        .commit();
            }
        }

    }

    @Override
    public void onCitySelected(City city, int position) {
        // Vamos a ver qué fragments tenemos cargados en la interfaz
        FragmentManager fm = getFragmentManager();
        CityPagerFragment cityPagerFragment = (CityPagerFragment) fm.findFragmentById(R.id.view_pager_fragment);

        if (cityPagerFragment != null) {
            // Tenemos un pager, vamos a ver cómo podemos decirle que cambie de ciudad
            cityPagerFragment.moveToCity(position);
        }
        else {
            // No tenemos un pager, lanzamos la actividad CityPagerActivity
            Intent intent = new Intent(this, CityPagerActivity.class);
            intent.putExtra(CityPagerActivity.EXTRA_CITY_INDEX, position);
            startActivity(intent);
        }
    }
}
